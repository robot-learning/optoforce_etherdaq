# etherdaq_ros

This is a fork from the [OptoForce/etherdaq_ros](https://github.com/OptoForce/etherdaq_ros) repo. The driver has been tested up to Ubuntu 16.04 with ROS Kinetic.

Installation
---------
Clone the repo into a catkin workspace and do the usual catkin process.

Usage
------------
To publish sensor readings to a ROS topic, run

`roslaunch optoforce_etherdaq_driver optoforce.launch`

You should see a roscore session load and it should be publishing sensed forces. To verify, in a new terminal window:

`rostopic echo /ethdaq_data`

You should see WrenchStamped messages being published at the rate specified in the launch file. If not, first check the IP address specified in the launch file and make sure it matches the static IP address printed on the EtherDaq device. If you still see nothing, put your device's IP address into a browser window, you should see the Optoforce interface. Go to the "Real-Time" page and ensure you see the sensor outputting values. If not, you need to troubleshoot your hardware setup.

Parameters
----------------------
* --help Shows the help screen and exits.
* --rate (default: 100) (in Hz) The publish speed of the F/T informations. It also sets the EtherDAQ speed to the given value. 
* --filter (default: 4) Set the filtering (Valid parameters: 0 = No filter; 1 = 500 Hz; 2 = 150 Hz; 3 = 50 Hz; 4 = 15 Hz; 5 = 5 Hz; 6 = 1.5 Hz)
* --address The IP address of the EtherDAQ device.
* --wrench  publish older Wrench message type instead of WrenchStamped
* --frame_id arg (default: "base_link") Frame ID for Wrench data


Topics
------------------
| Topic                | Description                                                                                                       |
|----------------------|-------------------------------------------------------------------------------------------------------------------|
| `/diagnostics`       | The status of the EtherDAQ (speed, last F/T values, system status, address, etc).                                 |
| `/etherdaq_data`     | F/T values are published either in Wrench or in WrenchStamped format if the force and torque units are given.     |
| `/etherdaq_data_raw` | F/T values are published either in Wrench or in WrenchStamped format if the force and torque units are not given. |

Services
------------------
The service `zero_force_sensor` allows your to account for the bias inherent to the sensor when no force is being applied to the sensor. To set the current sensor values as the zero point do

`rosservice call /zero_force_sensor true`

Sending `false` instead will set the bias to zero and you will be seeing the raw sensor values.


Description of Source Files
----------------------------------------------
| File                    | Description                                                           |                                                      
|-------------------------|-----------------------------------------------------------------------|       
|`etherdaq_driver.cpp`    | Implements the communication between EtherDAQ and ROS.                |
|`etherdaq_node.cpp`      | Publishs F/T values on ROS topics using etherdaq_driver.cpp services. |
|`etherdaq_subscriber.cpp`| An example node which subscribes to the topics of etherdaq_node and displays the actual F/T, speed and elapsed times between two packets. Also this node does a zero/unzero in every 10 seconds. |
