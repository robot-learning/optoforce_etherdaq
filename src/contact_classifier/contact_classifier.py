import rospy
import numpy as np


class ContactClassifier:

    def __init__(self, robot_name=None, window_size=None, mu_noise=None):
        if robot_name is None:
            robot_name = rospy.get_param("/robot_name")
        self.robot_name = robot_name
        if window_size is None:
            window_size = rospy.get_param("/%s/window_size" % self.robot_name)
        self.window_size = window_size
        if mu_noise is None:
            mu_noise = rospy.get_param("/%s/mu_noise" % self.robot_name)
        self.mu_noise = mu_noise        
        self.window = np.zeros((6, self.window_size))
        self.cached_contact = False
                                          
    def in_contact(self):
        in_contact = self.cached_contact
        try:
            mu_window = abs(np.mean(np.linalg.norm(self.window, axis=0)))
            in_contact = int(mu_window > self.mu_noise)
        except (AttributeError):
            rospy.logwarn("AttributeError: conjugate. Returning cached value: %r"
                          % self.cached_contact)
        return in_contact

    def get_contact_regions(self, data, gap=1):
        """
        Determine contact regions for a force profile.
        """
        in_contact = False # assume starting in freespace
        regions = []
        start = 0.0
        end = data.shape[1]
        for max_idx in range(int(self.window_size), data.shape[1], gap):
            min_idx = int(max_idx - self.window_size)
            idxs = range(min_idx, max_idx)
            w = data[:,idxs]
            mu = abs(np.mean(np.linalg.norm(w, axis=0))) 
            if not in_contact and mu > self.mu_noise:
                start = min_idx # is this what you want?
                in_contact = True
            if in_contact and mu <= self.mu_noise:
                end = max_idx # is this what you want?
                regions.append((start, end))
                in_contact = False
        return regions
    
    def update_window(self, wrench):
        self.window = np.roll(self.window, -1)
        self.window[:,-1] = wrench

